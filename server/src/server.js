/* eslint-disable no-console */
import http from 'http';
import fs from 'fs';
import express from 'express';
import socketIO from 'socket.io';
import socketHandler from './socket';
import errorHandlerMiddleware from './middlewares/errorHandlerMiddleware';
import { STATIC_PATH, PORT } from './config/serverConfig';

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

app.use(express.json());

socketHandler(io);

const staticPath = process.env.NODE_ENV === 'production'
  ? STATIC_PATH.production
  : STATIC_PATH.development;

app.use(express.static(staticPath));

app.get('*', (req, res) => {
  res.write(fs.readFileSync(`${staticPath}\\index.html`));
  res.end();
});

app.use(errorHandlerMiddleware);
console.log(PORT);
const port = process.env.NODE_ENV === 'production'
  ? process.env.PORT
  : PORT;

httpServer.listen(port, () => {
  console.log(`Listen server on port ${PORT}`);
});
