/* eslint-disable consistent-return */
/* eslint-disable no-console */
import countdown from './countdown';
import * as data from '../data';
import * as templatesMessage from '../templatesMessage';
import { END_DISTANCE_FOR_MESSAGE } from '../config/botConfig';
import driverSeat from '../helpers/driverSeat';

export default io => {
  io.on('connection', async socket => {
    let username = '';
    let roomname = '';

    socket.on('login', ({ login }, callback) => {
      if (data.hasUser(login)) {
        callback(`The name ${login} already exists`, { rooms: null });
        return;
      }
      username = login;
      data.addUser({ login });
      const responseRooms = data.getRoomsForShow();
      callback(null, { rooms: responseRooms });
    });

    socket.on('disconnect', async () => {
      data.deleteUserByName({ username });
      const startGame = data.removeUserToRoom({ username, roomName: roomname });
      await socket.leave(roomname);
      socket.broadcast.emit('user-leave', ({ username, startGame, name: roomname }));
      if (startGame) {
        const desiredRoom = data.addText({ roomName: roomname });
        countdown({ io, room: desiredRoom });
      }
      username = '';
      roomname = '';
    });

    socket.on('new-room', async ({ room: roomName }, callback) => {
      const rooms = data.getRooms();

      if (!roomName || (rooms.length && rooms.some(room => (room.name === roomName)))) {
        callback(null, { response: false });
        return;
      }

      roomname = roomName;
      const room = data.addRoom({ username, roomName });
      await socket.join(roomName);
      socket.broadcast.emit('create-room', { room });
      callback(null, { response: true });
    });

    socket.on('join', async ({ roomName }, callback) => {
      if (!data.getRoomUsersLimit({ roomName })) {
        return callback({ message: `Room ${roomName} has the maximum number of players.` });
      }

      data.addUserToRoom({ username, roomName });
      roomname = roomName;
      await socket.join(roomname);
      socket.broadcast.emit('user-join', { username, name: roomname });
      return callback(null);
    });

    socket.on('leave', async callback => {
      const startGame = data.removeUserToRoom({ username, roomName: roomname });

      await socket.leave(roomname);
      socket.broadcast.emit('user-leave', ({ username, startGame, name: roomname }));
      callback(null, { startGame });

      if (startGame) {
        const desiredRoom = data.addText({ roomName: roomname });
        countdown({ io, socket, room: desiredRoom });
      }
      roomname = '';
    });

    socket.on('ready-switch', callback => {
      const startGame = data.userReadySwitch({ username, roomName: roomname });
      socket.broadcast.emit('user-ready-switch', ({ username, startGame, name: roomname }));
      callback(null, { startGame });

      if (startGame) {
        const desiredRoom = data.addText({ roomName: roomname });
        countdown({ io, socket, room: desiredRoom });
      }
    });

    socket.on('letter', ({ username: userName }) => {
      const { endGame, userFinish, distance } = data.addLetter({ roomName: roomname, userName });

      socket.to(roomname).emit('add-letter', { username: userName });
      if (distance === END_DISTANCE_FOR_MESSAGE) {
        const usersPlace = driverSeat(roomname);
        if (!usersPlace) return '';
        if (usersPlace[0].username === username) {
          io.to(roomname).emit('message',
            { message: templatesMessage.message30CharactersBeforeFinish(usersPlace) });
        }
      }

      if (endGame) {
        io.to(roomname).emit('end-game');
        io.to(roomname).emit('end',
          { message: templatesMessage.finalResult(roomname) });
        socket.leave(roomname);
        data.deleteRoomByName({ roomName: roomname });
      }

      if (userFinish && !endGame) {
        io.to(roomname).emit('message',
          { message: templatesMessage.homeStretch(username) });
      }
    });

    socket.on('game-end', () => {
      socket.leave(roomname);
      roomname = '';
    });
  });
};
