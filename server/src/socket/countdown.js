/* eslint-disable no-console */
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  SECOND
} from '../config/socketConfig';

import {
  PERIOD_REPORTING_GAME_RESULTS
} from '../config/botConfig';

import * as templatesMessage from '../templatesMessage';
import * as data from '../data';

const countdown = async ({ io, socket, room }) => {
  let countDownValue = SECONDS_TIMER_BEFORE_START_GAME;
  io.to(room.name).emit('start-countdown', { users: room.users });
  const countDownCounter = setInterval(() => {
    if (countDownValue) {
      io.to(room.name).emit('countdown', { countDownValue });
    } else {
      clearInterval(countDownCounter);
      io.to(room.name).emit('game-text', { text: room.text });
      let timer = SECONDS_FOR_GAME;
      const gameTimer = setInterval(() => {
        timer -= 1;
        if (!(timer % PERIOD_REPORTING_GAME_RESULTS)) {
          io.to(room.name).emit('message',
            { message: templatesMessage.messageEvery30Seconds(room.name, timer) });
        }
        if (!timer) {
          clearInterval(gameTimer);
          io.to(room.name).emit('end-game');
          io.to(room.name).emit('end',
            { message: templatesMessage.finalResult(room.name) });
          socket.leave(room.name);
          data.deleteRoomByName({ roomName: room.name });
        }
      }, SECOND);
    }
    countDownValue -= 1;
  }, SECOND);
  io.to(room.name).emit('countdown', { countDownValue });
};

export default countdown;
