import * as data from '../data';

const driverSeat = roomName => {
  const room = data.getRoomByName(roomName);
  if (!room) return false;
  const { users, rating } = room;

  const racersOnWay = users
    .filter(user => !rating.some(({ username }) => username === user.username))
    .sort((userA, userB) => userB.progress - userA.progress);

  return [...rating, ...racersOnWay];
};

export default driverSeat;
