/* eslint-disable no-console */
/* eslint-disable indent */
import texts from '../../data';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config/socketConfig';

let users = [];
let rooms = [];

const getUsers = () => [...users];

const getRooms = () => [...rooms];

const getRoomsForShow = () => rooms.filter(room => !room?.text === true);

const hasUser = login => users.includes(login);

const addUser = ({ login }) => { users = [...users, login]; };

const deleteUserByName = ({ username }) => {
  users = users.filter(user => user !== username);
};

const addRoom = ({ roomName, username }) => {
  const room = {
    name: roomName,
    users: [
      {
        username,
        ready: false,
        progress: 0,
        finish: false
      }
    ],
    rating: []
  };

  rooms = [room, ...rooms];

  return room;
};

const getRoomUsersLimit = ({ roomName }) => rooms.some(room => {
  if (room.name !== roomName) return false;
  if (room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM) return true;
  return false;
});

const addUserToRoom = ({ roomName, username }) => {
  rooms = rooms.map(room => (room.name !== roomName
    ? room
    : {
      ...room,
      users: [
        ...room.users,
        {
          username,
          ready: false,
          progress: 0,
          finish: false
        }
      ]
    }
  ));
};

const removeUserToRoom = ({ username, roomName }) => {
  let startGame = false;
  rooms = rooms.map(room => {
    if (room.name !== roomName) return room;
    const rating = room.rating.filter(user => user.username !== username);

    const response = {
      ...room,
      rating,
      users: room.users.filter(user => user.username !== username)
    };
    if (response?.users.length > 1 && response?.users.every(({ ready }) => ready === true) && !room?.text) {
      startGame = true;
    }
    return response;
  }).filter(room => (room.users.length > 0));

  return startGame;
};

const addText = ({ roomName }) => {
  let textNumber = Math.round(texts.length * Math.random());
  textNumber = Math.max(0, textNumber);
  textNumber = Math.min(texts.length - 1, textNumber);
  const text = texts[textNumber];
  const textLength = text.length;
  let desiredRoom = {};
  rooms = rooms.map(room => {
    if (room.name !== roomName) return room;
    desiredRoom = { ...room, text };
    return { ...room, text, textLength, rating: [] };
  });
  return desiredRoom;
};

const userReadySwitch = ({ username, roomName }) => {
  let startGame = false;
  const updateUsers = users => {
    const response = users.map(user => (
      user.username !== username
        ? user
        : {
          ...user,
          ready: !user.ready
        }
    ));
    startGame = (response.length > 1 && response.every(({ ready }) => ready === true));
    return response;
  };

  rooms = rooms.map(room => (room.name !== roomName
    ? room
    : {
      ...room,
      users: updateUsers(room.users)
    }
  ));
  return startGame;
};

const addLetter = ({ roomName, userName }) => {
  let endGame = false;
  let finish = false;
  let distance = 0;
  rooms = rooms.map(room => {
    if (roomName !== room.name) return room;
    let { rating } = room;
    const response = {
      ...room,
      users: room.users.map(user => {
        if (user.username !== userName) return user;
        const progress = user.progress + 1;
        distance = room.text.length - progress;
        finish = progress === room.text.length;
        if (finish) rating = [...rating, { progress, username: user.username }];
        return { ...user, progress, finish };
      })
    };
    endGame = response.users.every(({ finish }) => finish);
    return { ...response, rating };
  });

  return { endGame, distance, userFinish: finish };
};

const deleteRoomByName = ({ roomName }) => {
  rooms = rooms.filter(({ name }) => name !== roomName);
};

const allUsersInRoomNotReady = (({ roomName }) => {
  rooms = rooms.map(room => {
    if (roomName !== room.name) return room;
    return {
      ...room,
      users: room.users.map(user => ({ ...user, ready: false, progress: 0 }))
    };
  });
});

const getRoomByName = roomName => rooms.find(({ name }) => name === roomName);

export {
  hasUser,
  getUsers,
  getRooms,
  getRoomsForShow,
  addUser,
  deleteUserByName,
  addRoom,
  getRoomUsersLimit,
  addUserToRoom,
  removeUserToRoom,
  addText,
  userReadySwitch,
  addLetter,
  deleteRoomByName,
  allUsersInRoomNotReady,
  getRoomByName
};
