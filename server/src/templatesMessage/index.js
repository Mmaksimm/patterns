import driverSeat from '../helpers/driverSeat';

const messageEvery30Seconds = (roomName, timeToEnd) => {
  const usersPlace = driverSeat(roomName);
  if (!usersPlace) return '';
  const messageLeadRacer = `${usersPlace[0].username} is in the lead in the race`;
  const messageRacer = index => {
    const gap = usersPlace[index - 1]?.progress - usersPlace[index]?.progress;
    if (!gap) {
      return `${usersPlace[index].username} takes place ${index + 1}`;
    }
    return `${usersPlace[index].username} is ${gap} character${(gap > 1) ? 's' : ''} behind`;
  };

  const messageRacers = usersPlace.map((user, index) => {
    if (index === 0) return messageLeadRacer;
    return messageRacer(index);
  }).join(', ');

  const timeEndRace = `, ${timeToEnd} second${(timeToEnd > 1) ? 's' : ''} to the end of the game.`;

  return `${messageRacers} ${timeEndRace}`;
};

const message30CharactersBeforeFinish = usersPlace => `
    There is very little left to the finish line and it looks like ${usersPlace[0].username}
    can cross it first. The second place can go to ${usersPlace[1].username}. But let's wait for the finish.
  `;

const homeStretch = username => `The finish line is crossed by ${username}.`;

const finalResult = roomName => {
  const usersPlace = driverSeat(roomName);
  if (!usersPlace) return '';

  return `
    Final result: ${usersPlace[0].username} takes first place
    ${usersPlace[1] ? `, ${usersPlace[1].username} second` : ''}
    ${usersPlace[2] ? `, ${usersPlace[2].username} third` : ''}.
  `;
};

export {
  messageEvery30Seconds,
  message30CharactersBeforeFinish,
  homeStretch,
  finalResult
};
