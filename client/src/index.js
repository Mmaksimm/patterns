/* eslint-disable no-console */
import router from './service/router';
import eventListener from './service/eventListener';
import socketListener from './service/socketListener';
import keyListener from './service/keyListener';
import login from './helpers/login';
import * as data from './service/data';
import * as render from './service/render';
import bot from './Bot';

import { path } from './constant';

import './styles/index.scss';

const start = async () => {
  const username = await data.getUsername();
  window.location.replace(path.login);
  if (username) {
    login(username);
  } else {
    render.login();
  }
};

document.addEventListener('click', eventListener);
document.addEventListener('keydown', keyListener);
window.addEventListener('hashchange', router);
socketListener();
bot();
start();
