const Countdown = countDownValue => `
  <span class="fs-1 font-weight text-uppercase">
    ${countDownValue}
  </span>`;

export default Countdown;
