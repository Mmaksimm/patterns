import { events } from '../constant';

const Login = `
  <div id="login-page" class="full-screen flex-centered">
    <div class="flex">
    <div class="username-input-container">
      <input autofocus placeholder="username" type="text">
    </div>
      <button id="submit-button" class="no-select" data-event=${events.login}>submit</button>
    </div>
  </div>
`;

export default Login;
