import { path } from '../constant';
import driverSeat from '../helpers/driverSeat';
import Bot from '../Bot/components/Bot';

const GameResults = () => {
  const resultList = driverSeat().map(username => `<li class="list-group-item fs-3">${username}</li>`).join('');

  const resultModal = `
    <div class="modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title">Game Results</h1>
            <a href="${path.rooms}" class="btn-close" aria-label="Close"  id="quit-results-btn"></a>
          </div>
          <div class="modal-body">
            <ol class="list-group list-group-numbered">
              ${resultList}
            </ol>
          </div>
          <div class="modal-footer">
            <a href="${path.rooms}" class="btn btn-light">Close</a>
          </div>
        </div>
      </div>
    </div>`;

  return `
    <div class="container-md h-100">
      <div class="row p-4 h-100">
        <div class="h-100 col-md-10"></div>
        ${Bot()}
      </div>
      ${resultModal}
    </div>`;
};

export default GameResults;
