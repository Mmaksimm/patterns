import AddRoom from './AddRoom';
import ListRooms from './ListRooms';

const RoomsPreview = rooms => (
  `
    <div class="container-md">
      ${AddRoom}
      ${ListRooms(rooms)}
    </div>`
);

export default RoomsPreview;
