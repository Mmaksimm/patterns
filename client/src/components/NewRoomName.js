import { path, events } from '../constant';

const NewRoomName = `
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Write the name of the new room.</h5>
        <a href=${path.rooms} class="btn-close" aria-label="Close"></a>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" name="roomName" id="new-room-name">
      </div>
      <div class="modal-footer">
        <a href=${path.rooms} class="btn btn-light">Cancel</a>
        <button class="btn btn-secondary" data-event=${events.createRoom}>Create</button>
      </div>
    </div>
  </div>
`;

export default NewRoomName;
