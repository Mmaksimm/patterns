/* eslint-disable no-console */
import RoomPreview from './RoomPreview';

const ListRooms = rooms => {
  const listRooms = rooms.map(room => RoomPreview(room)).join('');

  return `
  <div class="row py-3">
    ${listRooms}
  </div>`;
};

export default ListRooms;
