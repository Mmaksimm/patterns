const SecondsLeft = seconds => `
<span class="fs-4 font-weight">
  ${seconds} seconds left
</span>
`;

export default SecondsLeft;
