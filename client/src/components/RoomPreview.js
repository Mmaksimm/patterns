const RoomPreview = ({ name = '', users = [] }) => `
  <div class="col-md-3 my-4">
    <div class="card">
      <div class="card-body">
        <p class="card-text fs-6 fst-italic">
          ${users.length} user${users.length > 1 ? 's' : ''} connected.
        </p>
        <h3 class="text-center">${name}</h3>
        <p class="card-text text-center">
          <button class="btn btn-secondary" data-event="game-${name}">Join</button>
        </p>
      </div>
    </div>
  </div>
`;

export default RoomPreview;
