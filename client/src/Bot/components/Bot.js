import { messageData } from '../service/data';

const Bot = () => `
  <div class="h-100 col-md-2 bot-wrapper">
    <div class="bot">
      <div id="bot">
        ${messageData.get()}
      </div>
    </div>
    <div class="bot-image"></div>
  </div>`;

export default Bot;
