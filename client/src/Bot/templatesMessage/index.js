import { storages } from '../../constant';

const introducedItself = () => 'Hey! I\'m a bot commentator. Today I will comment on the race on the keyboard.';

const startRace = users => {
  const usersLength = users.length;
  if (!usersLength) return '';

  const username = sessionStorage.getItem(storages.username);

  const players = users
    .sort((userA, userB) => {
      if (userB.username === username) return 1;
      if (userA.username === username) return -1;
      if (userA.username > userB.username) return 1;
      return -1;
    });

  const transports = [
    'on a cool keyboard',
    'on a gaming keyboard',
    'on a sports keyboard',
    'on a pumped-up keyboard',
    'on a powerful keyboard'
  ];

  const phrasePresentationPlayers = players.map(
    ({ username }, index) => `${username} ${transports[index]}${index < (usersLength - 1) ? ',' : ''}`
  ).join(' ');

  return `${phrasePresentationPlayers}  take part in the race.`;
};

export {
  introducedItself,
  startRace
};
