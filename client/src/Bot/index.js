import socketListener from './service/socketListener';

const bot = () => { socketListener(); };

export default bot;
