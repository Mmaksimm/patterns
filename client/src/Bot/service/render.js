import { messageData } from './data';

const botRender = () => {
  const bot = document.getElementById('bot');
  bot.innerHTML = messageData.get();
};

export default botRender;
