// Facade
import socket from '../../helpers/socket';
import * as service from './service';

const socketListener = () => {
  socket.on('start-countdown', service.start);
  socket.on('message', service.message);
  socket.on('end', service.endGame);
};

export default socketListener;
