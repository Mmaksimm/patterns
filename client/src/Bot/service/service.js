/* eslint-disable no-param-reassign */
import { startRace } from '../templatesMessage';
import { messageData, endGameData } from './data';
import botRender from './render';

const start = ({ users }) => {
  const message = startRace(users);
  endGameData.set(false);
  messageData.set(message);
  botRender(startRace(users));
};

// Proxy
const message = ({ message }) => {
  const proxyMessage = new Proxy(messageData, {
    set(target, property, value) {
      if (endGameData.get()) return false;
      target[property] = value;
      return true;
    }
  });

  try {
    proxyMessage.set(message);
  } catch {
    return;
  }
  botRender();
};

const endGame = value => {
  message(value);
  endGameData.set(true);
};

export {
  start,
  message,
  endGame
};
