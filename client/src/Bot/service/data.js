/* eslint-disable class-methods-use-this */
/* eslint-disable lines-between-class-members */
/* eslint-disable no-undef */
// Factory
// eslint-disable-next-line max-classes-per-file
import { introducedItself } from '../templatesMessage';

class Data {
  message() {
    return class Message {
      constructor(message) {
        this.message = message;
      }

      get() {
        return this.message;
      }
      set(messageNew) {
        this.message = messageNew;
      }
    };
  }

  endGame() {
    return {
      endGame: false,
      get: () => this.endGame,
      set: value => {
        this.endGame = value;
      }
    };
  }

  create(methodName) {
    switch (methodName) {
      case 'message':
        return this.message();

      case 'endGame':
        return this.endGame();

      default:
        return {};
    }
  }
}

const data = new Data();

const MessageData = data.create('message');
const endGameData = data.create('endGame');
const messageData = new MessageData(introducedItself());

export {
  messageData,
  endGameData
};
