import { events } from '../constant';
import * as roomService from './roomService';
import login from '../helpers/login';

const eventListener = event => {
  if (!event.target?.dataset?.event) return;
  event.preventDefault();
  const eventName = event.target.dataset.event;

  switch (eventName) {
    case events.login:
      login();
      break;

    case events.createRoom:
      roomService.createRoom();
      break;

    case events.leaveRoom:
      roomService.leaveRoom();
      break;

    case events.ready:
      roomService.readySwitch({});
      break;

    default:
      roomService.joinRoom(eventName.split('-')[1]);
      break;
  }
};

export default eventListener;
