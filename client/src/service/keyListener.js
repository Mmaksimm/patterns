/* eslint-disable default-case */
/* eslint-disable consistent-return */
import * as data from './data';
import { path } from '../constant';
import login from '../helpers/login';
import * as roomService from './roomService';

const keyListener = ({ key }) => {
  if (data?.getRoom()?.text?.length) return roomService.gameHandler({ key });
  if (key !== 'Enter') return;
  switch (window.location.hash) {
    case path.login:
      login();
      break;

    case path.new:
      roomService.createRoom();
      break;
  }
};

export default keyListener;
