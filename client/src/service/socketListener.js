import socket from '../helpers/socket';
import * as roomService from './roomService';
import * as render from './render';

const socketListener = async () => {
  socket.on('create-room', roomService.socketCreateRoom);

  socket.on('user-join', roomService.socketUserJoin);

  socket.on('user-leave', roomService.socketUserLeave);

  socket.on('user-ready-switch', roomService.userReadySwitch);

  socket.on('countdown', render.countDown);

  socket.on('game-text', roomService.gameText);

  socket.on('add-letter', roomService.socketAddLetter);

  socket.on('end-game', roomService.socketEndGame);
};

export default socketListener;
