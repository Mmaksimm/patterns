/* eslint-disable no-console */

import { path } from '../constant';
import * as data from '../service/data';
import socket from './socket';

const login = async (username = false) => {
  try {
    const inputValue = document.getElementsByTagName('input')[0]?.value?.trim() || username;
    if (!inputValue) {
      window.location.replace(path.login);
      return;
    }
    const login = username || inputValue;
    socket.emit('login', { login }, async (error, { rooms }) => {
      if (!error && rooms) {
        await data.setUsername(inputValue);
        await data.setRooms(rooms);
        window.location.replace(path.rooms);
      } else {
        alert(`The name ${inputValue} already exists. Please choose another name.`);
      }
    });
  } catch (err) {
    alert(err.message);
    window.location.replace(path.login);
  }
};

export default login;
