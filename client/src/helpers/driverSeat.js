import * as data from '../service/data';

const driverSeat = () => {
  const { users } = data.getRoom();
  const rating = data.getRating();

  const racersOnWay = users
    .filter(user => !rating.some(userName => userName === user.username))
    .sort((userA, userB) => userB.progress - userA.progress)
    .map(({ username }) => username);

  return [...rating, ...racersOnWay];
};

export default driverSeat;
