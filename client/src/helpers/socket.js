import io from 'socket.io-client';

const url = 'http://localhost:3002';
const socket = process.env.NODE_ENV === 'production'
  ? io()
  : io(url);

export default socket;
